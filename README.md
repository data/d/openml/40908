# OpenML dataset: Pen_global

https://www.openml.org/d/40908

**WARNING: This dataset is still in preparation.**

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.csv`](./dataset/tables/data.csv): CSV file with data
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

&quot;This UCI dataset contains the hand- written digits 0&ndash;9 of 45 different writers. Here, in the &ldquo;global&rdquo; task, we only keep the digit 8 as the normal class and sample the 10 digits from all of the other classes as anomalies. This results in one big normal cluster and global anomalies sparsely distributed. The resulting pen-global dataset has 16 dimensions and 809 instances including a large amount of anomalies (11.1%).&quot; (cite from Goldstein, Markus, and Seiichi Uchida. &quot;A comparative evaluation of unsupervised anomaly detection algorithms for multivariate data.&quot; PloS one 11.4 (2016): e0152173). This dataset is not the original dataset. The target variable &quot;Target&quot; is relabeled into &quot;Normal&quot; and &quot;Anomaly&quot;.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/40908) of an [OpenML dataset](https://www.openml.org/d/40908). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/40908/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/40908/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/40908/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

